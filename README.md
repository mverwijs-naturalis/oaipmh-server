# oaipmh-server

A DropWizard application that exposes OAI repositories through a REST interface. The oaipmh-server library has a dependency on the oaipmh-api library and on any concrete implementation of the API provided by oaipmh-api. Currently that's just geneious-oaipmh.

### Creating a distributable

Just run

```
mvn clean install
```

This will not just do what you expect from this maven command, but also create a tar file containing the oaipmh-server library and its dependencies, plus a start/stop script (oaipmh-server.sh), plus the server configuration file (oaipmh-server.yaml). The tar file is created directly under the maven output directory ("target") and can be sent off to infra & network from there.

Make sure though that you have fresh builds of oaipmh-api, geneious-oaipmh and naturalis-common. These are separate Maven projects, but it's likely that you will develop at least the first two in tandem with oaipmh-server. If you don't build them first (using `mvn clean install`) you will end up with older versions of them in the tar file.

### Installing and running the server

- Unpack the tar file. This will create a directory named oaipmh-server containing the start/stop script, the server configuration file and a "lib" directory.
- Edit oaipmh-server.yaml There is only one setting in config.yaml that **must** be changed: the password(s) for the databases. Other settings can be changed or accepted as desired.
- From the command line run:

```
# To start the server:
$ ./oaipmh-server.sh start
# To stop the server:
$ ./oaipmh-server.sh stop
# To get the server status:
$ ./oaipmh-server.sh status
```



