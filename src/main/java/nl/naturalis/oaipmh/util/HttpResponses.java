package nl.naturalis.oaipmh.util;

import java.io.InputStream;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.naturalis.common.exception.ExceptionSource;

import static javax.ws.rs.core.Response.Status.INTERNAL_SERVER_ERROR;
import static javax.ws.rs.core.Response.Status.NOT_FOUND;
import static javax.ws.rs.core.Response.Status.OK;

import static nl.naturalis.common.ExceptionMethods.getRootStackTraceAsString;

/**
 * Shortcuts for generating {@link Response} objects.
 * 
 * @author Ayco Holleman
 *
 */
public class HttpResponses {

  private static final Logger logger = LoggerFactory.getLogger(HttpResponses.class);

  private HttpResponses() {}

  /**
   * Generates an HTTP response with status 404 (Not Found) and the specified message in the response body. The content type of the response
   * body is set to text/plain.
   * 
   * @param message
   * @return
   */
  public static Response notFound(String message) {
    String msg = getMessage(NOT_FOUND, message);
    logger.error(msg);
    return plainTextResponse(NOT_FOUND, msg);
  }

  /**
   * Generates an HTTP response with status 500 (Internal Server Error) and the specified message in the response body. The content type of
   * the response body is set to text/plain.
   * 
   * @param message
   * @return
   */
  public static Response serverError(String message) {
    String msg = getMessage(INTERNAL_SERVER_ERROR, message);
    logger.error(msg);
    return plainTextResponse(INTERNAL_SERVER_ERROR, msg);
  }

  /**
   * Generates an HTTP response with status 500 (INTERNAL SERVER ERROR) and the stack trace of the <b>root cause</b> of the specified
   * exception in the response body.
   * 
   * @param t
   * @return
   */
  public static Response serverError(Exception t) {
    ExceptionSource es = new ExceptionSource(t, "nl.naturalis");
    String msg = getMessage(INTERNAL_SERVER_ERROR, es.getDetailedMessage());
    logger.error(msg);
    return plainTextResponse(INTERNAL_SERVER_ERROR, msg + '\n' + getRootStackTraceAsString(t));
  }

  /**
   * Generates a 200 (OK) response with the specified message in the response body and a Content-Type header of text/plain.
   * 
   * @param message
   * @return
   */
  public static Response plainTextResponse(String message) {
    return plainTextResponse(OK, message);
  }

  /**
   * Generates a response with the specified HTTP status code, the specified message in the reponse body and a Content-Type header of
   * text/plain.
   * 
   * @param status
   * @param message
   * @return
   */
  public static Response plainTextResponse(Status status, String message) {
    return Response.status(status).entity(message).type(MediaType.TEXT_PLAIN).build();
  }

  /**
   * Generate a 200 (OK) response with a Content-Type header of application/xml and the specified XML string in the response body.
   * 
   * @param xml
   * @return
   */
  public static Response xmlResponse(String xml) {
    return xmlResponse(OK, xml);
  }

  /**
   * Generate a 200 (OK) response with a Content-Type header of application/xml and the specified JAXB object converted to XML in the
   * response body.
   * 
   * @param jaxbObject
   * @return
   */
  public static Response xmlResponse(Object jaxbObject) {
    return Response.ok(jaxbObject, MediaType.APPLICATION_XML).build();
  }

  /**
   * Returns XML content with the provided (possibbly non-OK) status. This can be used to apply HTTP semantics for invalid OAI-PMH request.
   * 
   * @param jaxbObject
   * @return
   */
  public static Response xmlResponse(Status status, String xml) {
    return Response
        .status(status)
        .entity(xml)
        .type(MediaType.APPLICATION_XML)
        .build();
  }

  /**
   * Returns XML content with the provided (possibbly non-OK) status. This can be used to apply HTTP semantics for invalid OAI-PMH request.
   * 
   * @param jaxbObject
   * @return
   */
  public static Response xmlResponse(Status status, Object jaxbObject) {
    return Response
        .status(status)
        .entity(jaxbObject)
        .type(MediaType.APPLICATION_XML)
        .build();
  }

  /**
   * Reads the contents of the specified input stream and puts it on the HTTP outputstream.
   * 
   * @param is
   * @param mediaType
   * @return
   */
  public static Response streamingResponse(InputStream is, String mediaType) {
    return Response.ok().type(mediaType).entity(is).build();
  }

  /**
   * Reads the contents of the specified input stream and puts it on the HTTP outputstream.
   * 
   * @param is
   * @param mediaType
   * @return
   */
  public static Response streamingResponse(InputStream is, MediaType mediaType) {
    return Response.ok().type(mediaType).entity(is).build();
  }

  private static String getMessage(Status status, String message) {
    return String.format("%d (%s) *** %s", status.getStatusCode(), status.getReasonPhrase(), message);
  }

}
