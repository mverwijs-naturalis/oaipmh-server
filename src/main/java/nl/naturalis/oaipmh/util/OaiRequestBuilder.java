package nl.naturalis.oaipmh.util;

import java.net.URI;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Optional;

import javax.ws.rs.core.UriInfo;

import org.openarchives.oai._2.OAIPMHerrorType;
import org.openarchives.oai._2.VerbType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.naturalis.common.StringMethods;
import nl.naturalis.common.time.FuzzyDate;
import nl.naturalis.common.time.FuzzyDateException;
import nl.naturalis.common.time.FuzzyDateParser;
import nl.naturalis.common.time.ParseSpec;
import nl.naturalis.oaipmh.api.Argument;
import nl.naturalis.oaipmh.api.ArgumentValidator;
import nl.naturalis.oaipmh.api.ArgumentValidatorFactory;
import nl.naturalis.oaipmh.api.BadArgumentError;
import nl.naturalis.oaipmh.api.BadResumptionTokenException;
import nl.naturalis.oaipmh.api.BadVerbError;
import nl.naturalis.oaipmh.api.OaiRequest;
import nl.naturalis.oaipmh.api.ResumptionTokenParser;

import static nl.naturalis.common.StringMethods.appendIfAbsent;
import static nl.naturalis.common.StringMethods.isBlank;
import static nl.naturalis.oaipmh.api.Argument.FROM;
import static nl.naturalis.oaipmh.api.Argument.IDENTIFIER;
import static nl.naturalis.oaipmh.api.Argument.METADATA_PREFIX;
import static nl.naturalis.oaipmh.api.Argument.RESUMPTION_TOKEN;
import static nl.naturalis.oaipmh.api.Argument.SET;
import static nl.naturalis.oaipmh.api.Argument.UNTIL;
import static nl.naturalis.oaipmh.api.Argument.VERB;

/**
 * Builds {@link OaiRequest} objects from HTTP requests.
 * 
 * @author Ayco Holleman
 *
 */
public class OaiRequestBuilder {

  private static final Logger logger = LoggerFactory.getLogger(OaiRequestBuilder.class);

  private static final FuzzyDateParser dateParser = new FuzzyDateParser(
      ParseSpec.ISO_INSTANT,
      ParseSpec.ISO_LOCAL_DATE);

  private final ResumptionTokenParser parser;
  private final String baseUrlOverride;
  private final UriInfo uriInfo;

  private OaiRequest request;
  private List<OAIPMHerrorType> errors;

  public OaiRequestBuilder(ResumptionTokenParser parser, String baseUrlOverride, UriInfo uriInfo) {
    this.parser = parser;
    this.baseUrlOverride = baseUrlOverride;
    this.uriInfo = uriInfo;
  }

  /**
   * Creates an {@code OaiRequest} instance from the specified {@link UriInfo} object.
   * 
   * @param uriInfo
   * @return
   */
  public OaiRequest build() {
    this.request = new OaiRequest();
    this.errors = new ArrayList<>();
    request.setBaseUri(getBaseUri());
    request.setRequestUri(getRequestUri());
    request.setMetadataPrefix(getArg(METADATA_PREFIX));
    request.setIdentifier(getArg(IDENTIFIER));
    request.setSet(getArg(SET));
    request.setResumptionToken(getArg(RESUMPTION_TOKEN));
    setVerb();
    setFrom();
    setUntil();
    checkArguments();
    parseResumptionToken();
    return request;
  }

  /**
   * Return the errors encountered while building the {@code OAIPMHRequest} object.
   * 
   * @return
   */
  public List<OAIPMHerrorType> getErrors() {
    return errors;
  }

  private void setVerb() {
    String arg = getArg(VERB);
    if (arg == null) {
      errors.add(new BadVerbError());
    } else {
      try {
        VerbType verb = VerbType.fromValue(arg);
        request.setVerb(verb);
      } catch (IllegalArgumentException e) {
        errors.add(new BadVerbError(arg));
      }
    }
  }

  private void setFrom() {
    String arg = getArg(FROM);
    if (arg != null) {
      try {
        FuzzyDate date = dateParser.parse(arg);
        request.setFrom(date.toInstant());
        request.setDateFormatFrom(date.getParseSpec().getFormatter());
      } catch (FuzzyDateException e) {
        errors.add(BadArgumentError.badDate(arg));
      }
    }
  }

  private void setUntil() {
    String arg = getArg(UNTIL);
    if (arg != null) {
      try {
        FuzzyDate date = dateParser.parse(arg);
        request.setUntil(date.toInstant());
        request.setDateFormatUntil(date.getParseSpec().getFormatter());
      } catch (FuzzyDateException e) {
        errors.add(BadArgumentError.badDate(arg));
      }
    }
  }

  /*
   * Check for duplicate and illegal parameters. Returns all valid OAI-PMH arguments in the request except the verb, which is more like the
   * name of the function to be executed using those arguments.
   */
  private void checkArguments() {
    EnumSet<Argument> args = EnumSet.noneOf(Argument.class);
    for (String param : uriInfo.getQueryParameters().keySet()) {
      Argument arg = Argument.parse(param);
      if (arg == null) {
        String msg = "Illegal argument: " + param;
        errors.add(new BadArgumentError(msg));
      } else {
        List<String> values = uriInfo.getQueryParameters().get(param);
        if (values == null || values.size() == 0) {
          String msg = "Empty argument: " + param;
          errors.add(new BadArgumentError(msg));
        } else if (values.size() != 1) {
          String msg = "Duplicate argument: " + param;
          errors.add(new BadArgumentError(msg));
        }
        if (arg != VERB) {
          args.add(arg);
        }
      }
    }
    if (request.getVerb() != null) {
      ArgumentValidatorFactory acf = ArgumentValidatorFactory.INSTANCE;
      Optional<ArgumentValidator> validator = acf.getValidatorForVerb(request.getVerb());
      if (validator.isEmpty()) {
        errors.add(BadArgumentError.verbNotSupported(request.getVerb(), acf.getSupportedVerbs()));
      } else {
        errors.addAll(validator.get().validate(args));
      }
    }
  }

  private void parseResumptionToken() {
    if (request.getResumptionToken() != null) {
      try {
        parser.parseResumptionToken(request);
      } catch (BadResumptionTokenException e) {
        errors.add(e.getErrors().get(0));
      }
    }
  }

  private URI getBaseUri() {
    return isBlank(baseUrlOverride) ? uriInfo.getBaseUri() : URI.create(baseUrlOverride);
  }

  private URI getRequestUri() {
    if (isBlank(baseUrlOverride)) {
      return uriInfo.getRequestUri();
    }
    /*
     * The base URL was hard-coded in the server configuration file, probably because the OAI-PMH server was unable to figure out the base
     * URL by itself. Maybe there's firewall or load balancer in front of the OAI-PMH server.
     */
    String s = appendIfAbsent(baseUrlOverride, "/") + uriInfo.getPath();
    return URI.create(s);
  }

  private String getArg(Argument arg) {
    return logger.isDebugEnabled() ? getArgDebug(arg) : getArgNoDebug(arg);
  }

  private String getArgDebug(Argument arg) {
    StringBuilder sb = new StringBuilder(50);
    sb.append("Retrieving URL parameter ").append(arg.param()).append(": ");
    String s = null;
    if (uriInfo.getQueryParameters().containsKey(arg.param())) {
      s = uriInfo.getQueryParameters().getFirst(arg.param());
      if (s == null) {
        sb.append("null");
      } else if ((s = s.strip()).isEmpty()) {
        sb.append("\"\" (empty string, evaluates to null)");
        s = null;
      } else {
        sb.append('"').append(s).append('"');
      }
    } else {
      sb.append("absent");
    }
    logger.debug(sb.toString());
    return s;
  }

  private String getArgNoDebug(Argument arg) {
    String s = uriInfo.getQueryParameters().getFirst(arg.param());
    return StringMethods.isBlank(s) ? null : s;
  }

}
