package nl.naturalis.oaipmh.util;

import java.io.IOException;
import java.io.OutputStream;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.StreamingOutput;

import org.openarchives.oai._2.OAIPMHtype;
import org.openarchives.oai._2.VerbType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.naturalis.oaipmh.api.OaiException;
import nl.naturalis.oaipmh.api.OaiRepository;
import nl.naturalis.oaipmh.api.OaiRequest;

import static org.openarchives.oai._2.VerbType.GET_RECORD;
import static org.openarchives.oai._2.VerbType.IDENTIFY;
import static org.openarchives.oai._2.VerbType.LIST_IDENTIFIERS;
import static org.openarchives.oai._2.VerbType.LIST_METADATA_FORMATS;
import static org.openarchives.oai._2.VerbType.LIST_RECORDS;
import static org.openarchives.oai._2.VerbType.LIST_SETS;

import static nl.naturalis.oaipmh.api.util.OaiUtil.createOAIPMHErrorResponse;

/**
 * Executes the appropriate method on an {@link OaiRepository} depending on the {@link OaiRequest}, and then forwards the response from the
 * repository to the client.
 * 
 * @author Ayco Holleman
 *
 */
public class RequestExecutor implements StreamingOutput {

  private static final Logger logger = LoggerFactory.getLogger(RequestExecutor.class);

  private final OaiRequest request;
  private final OaiRepository repo;

  public RequestExecutor(OaiRepository repository, OaiRequest request) {
    this.request = request;
    this.repo = repository;
  }

  /**
   * Returns the OAI-PMH to the client.
   * 
   * @return
   */
  public Response getResponse() {
    return HttpResponses.xmlResponse(this);
  }

  @Override
  public void write(OutputStream out) throws IOException, WebApplicationException {
    VerbType verb = request.getVerb();
    logger.info("Executing {} request", verb.value());
    try {
      if (verb == GET_RECORD) {
        repo.getRecord(out);
      } else if (verb == IDENTIFY) {
        repo.identify(out);
      } else if (verb == LIST_IDENTIFIERS) {
        repo.listIdentifiers(out);
      } else if (verb == LIST_METADATA_FORMATS) {
        repo.listMetaDataFormats(out);
      } else if (verb == LIST_RECORDS) {
        repo.listRecords(out);
      } else if (verb == LIST_SETS) {
        repo.listSets(out);
      }
    } catch (OaiException e) {
      OAIPMHtype oaipmh = createOAIPMHErrorResponse(request, e.getErrors());
      throw new WebApplicationException(HttpResponses.xmlResponse(oaipmh));
    } catch (UnsupportedOperationException e) {
      throw new WebApplicationException(e.getMessage(), Status.NOT_IMPLEMENTED);
    } catch (Exception e) {
      throw new WebApplicationException(e);
    }
  }

}
