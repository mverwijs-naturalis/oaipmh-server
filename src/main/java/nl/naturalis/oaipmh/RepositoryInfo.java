package nl.naturalis.oaipmh;

import java.util.Map;

import nl.naturalis.oaipmh.api.OaiRepository;
import nl.naturalis.oaipmh.api.RepositoryGroup;
import nl.naturalis.oaipmh.api.StandAloneRepository;

/**
 * Provides the OAI server information about how to initialize a repository or repository group. This object is embedded within the over-all
 * configuration for the OAI server.
 *
 * @author Ayco Holleman
 */
public class RepositoryInfo {

  private String javaClass;
  private Map<String, Object> config;

  /**
   * Returns the fully-qualified name of the Java class of the {@link OaiRepository} or {@link RepositoryGroup}.
   * 
   * @return
   */
  public String getJavaClass() {
    return javaClass;
  }

  public void setJavaClass(String javaClass) {
    this.javaClass = javaClass;
  }

  /**
   * Returns the configuration for the {@link OaiRepository} or {@link RepositoryGroup}. The configuration for the repositories is emebedded
   * within the over-all configuration for the OAI server. The repository configuration is returned as a raw Map, because the OAI server
   * does not know anything about the particular {@code OaiRepository} implementation it is dealing with, let alone its configuration. Once
   * the OAI server has instantiated a repository using {@link #getJavaClass() getJavaClass}, it can ask it which configuration class it
   * uses by calling its {@link StandAloneRepository#getConfigClass() getConfigClass} method, and then ask Jackson to convert the map to an
   * instance of that class.
   * 
   * @return
   */
  public Map<String, Object> getConfig() {
    return config;
  }

  public void setConfig(Map<String, Object> config) {
    this.config = config;
  }

}
