package nl.naturalis.oaipmh;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.dropwizard.lifecycle.Managed;
import io.dropwizard.setup.Environment;
import nl.naturalis.oaipmh.api.RepositoryConfig;
import nl.naturalis.oaipmh.api.RepositoryGroup;
import nl.naturalis.oaipmh.api.RepositoryInitializationException;
import nl.naturalis.oaipmh.api.StandAloneRepository;

/*
 * Does the dirty work of instantiating the repositories and repository groups. It's pointless to cling to generics here because the OAI
 * server doesn't know or care about the repositories running inside it, and never calls repository-specific methods.
 */
@SuppressWarnings({"rawtypes", "unchecked"})
class RepositoryInitializer {

  private static final Logger logger = LoggerFactory.getLogger(RepositoryInitializer.class);

  private static final ObjectMapper om = new ObjectMapper();

  private final Environment env;

  RepositoryInitializer(Environment env) {
    this.env = env;
  }

  Map<String, StandAloneRepository<?>> getRepositories(Map<String, RepositoryInfo> cfg) throws Exception {
    if (cfg == null) {
      return Collections.emptyMap();
    }
    Map<String, StandAloneRepository<?>> repos = new LinkedHashMap<>();
    for (Map.Entry<String, RepositoryInfo> e : cfg.entrySet()) {
      String name = e.getKey(); // The name of the repository
      RepositoryInfo info = e.getValue();
      Class clazz; // The class of the OaiRepository implementation
      try {
        clazz = Class.forName(info.getJavaClass());
      } catch (ClassNotFoundException e1) {
        throw noSuchClass(name, info, true);
      }
      StandAloneRepository instance;
      try {
        instance = (StandAloneRepository) clazz.getConstructor().newInstance();
      } catch (NoSuchMethodException e2) {
        throw missingNoArgConstructor(name, info, true);
      }
      RepositoryConfig repoConfig;
      try {
        repoConfig = (RepositoryConfig) om.convertValue(info.getConfig(), instance.getConfigClass());
      } catch (IllegalArgumentException e3) {
        throw badConfiguration(name, instance.getConfigClass(), false);
      }
      env.lifecycle().manage(new Managed() {
        @Override
        public void start() throws Exception {
          logger.info("Initializing OAI repository {}", name);
          instance.configure(repoConfig);
        }

        @Override
        public void stop() throws Exception {
          logger.info("Starting shutdown of OAI repository \"{}\"", name);
          instance.tearDown();
          logger.info("Shutdown of OAI repository {} complete", name);
        }
      });
      repos.put(name, instance);
    }
    return repos;
  }

  Map<String, RepositoryGroup<?>> getRepoGroups(Map<String, RepositoryInfo> cfg) throws Exception {
    if (cfg == null) {
      return Collections.emptyMap();
    }
    Map<String, RepositoryGroup<?>> groups = new LinkedHashMap<>();
    for (Map.Entry<String, RepositoryInfo> e : cfg.entrySet()) {
      String name = e.getKey();
      RepositoryInfo info = e.getValue();
      Class repoClass;
      try {
        repoClass = Class.forName(info.getJavaClass());
      } catch (ClassNotFoundException e1) {
        throw noSuchClass(name, info, true);
      }
      RepositoryGroup instance;
      try {
        instance = (RepositoryGroup) repoClass.getConstructor().newInstance();
      } catch (NoSuchMethodException e2) {
        throw missingNoArgConstructor(name, info, true);
      }
      RepositoryConfig repoConfig;
      try {
        repoConfig = (RepositoryConfig) om.convertValue(info.getConfig(), instance.getConfigClass());
      } catch (IllegalArgumentException e3) {
        throw badConfiguration(name, instance.getConfigClass(), true);
      }
      env.lifecycle().manage(new Managed() {
        @Override
        public void start() throws Exception {
          logger.info("Initializing repository group {}", name);
          instance.configure(repoConfig);
        }

        @Override
        public void stop() throws Exception {
          logger.info("Starting shutdown of repository group {}", name);
          instance.tearDown();
          logger.info("Shutdown of repository group {} complete");
        }
      });
      groups.put(name, instance);
    }
    return groups;
  }

  private static RepositoryInitializationException noSuchClass(String name, RepositoryInfo info, boolean isGroup) {
    String fmt = "Error while initializing repository%s %s. Class not found: %s";
    String group = isGroup ? " group" : "";
    String msg = String.format(fmt, group, name, info.getJavaClass());
    return new RepositoryInitializationException(msg);
  }

  private static RepositoryInitializationException missingNoArgConstructor(String name, RepositoryInfo info, boolean isGroup) {
    String fmt = "Error while initializing repository%s %s. Missing no-arg constructor in %s";
    String group = isGroup ? " group" : "";
    String msg = String.format(fmt, group, name, info.getJavaClass());
    return new RepositoryInitializationException(msg);
  }

  private static RepositoryInitializationException badConfiguration(String name, Class configClass, boolean isGroup) {
    String fmt = "Error while initializing repository%s %s. The contents of the \"config\" element is not compatible with %s";
    String group = isGroup ? " group" : "";
    String msg = String.format(fmt, group, name, configClass);
    return new RepositoryInitializationException(msg);
  }

}
