package nl.naturalis.oaipmh;

import java.util.Map;

import javax.ws.rs.core.UriInfo;

import io.dropwizard.Configuration;
import nl.naturalis.oaipmh.api.RepositoryConfig;

/**
 * Represents the general configuration for the OAI server. The general configuration contains repository-specific blocks modeled by
 * {@link RepositoryConfig} implementations.
 *
 * @author Ayco Holleman
 */
public class OaiConfig extends Configuration {

  private String baseUrl;
  private Map<String, RepositoryInfo> repositories;
  private Map<String, RepositoryInfo> repositorGroups;

  /**
   * Returns the base URL under which the OAI server runs. Ordinarily the service can figure this out itself (by calling
   * {@link UriInfo#getBaseUri() UriInfo.getBaseUri}). However, if the service runs behind a load balancer or firewall, it could be that the
   * server sees a different base URL than the client. You must then explicitly configure the base URL that the client sees in the server
   * configuration file because OAI repositories need it to generate xsi:schemaLocation attributes.
   * 
   * @return
   */
  public String getBaseUrl() {
    return baseUrl;
  }

  public void setBaseUrl(String baseUrl) {
    this.baseUrl = baseUrl;
  }

  public Map<String, RepositoryInfo> getRepositories() {
    return repositories;
  }

  public void setRepositories(Map<String, RepositoryInfo> repositories) {
    this.repositories = repositories;
  }

  public Map<String, RepositoryInfo> getRepositoryGroups() {
    return repositorGroups;
  }

  public void setRepositoryGroups(Map<String, RepositoryInfo> resourceGroups) {
    this.repositorGroups = resourceGroups;
  }

}
