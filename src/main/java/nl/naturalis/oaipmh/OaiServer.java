package nl.naturalis.oaipmh;

import java.util.Map;

import io.dropwizard.Application;
import io.dropwizard.setup.Environment;
import nl.naturalis.oaipmh.api.RepositoryGroup;
import nl.naturalis.oaipmh.api.StandAloneRepository;
import nl.naturalis.oaipmh.resource.HomeResource;
import nl.naturalis.oaipmh.resource.RepoGroupResource;
import nl.naturalis.oaipmh.resource.RepositoryResource;

public class OaiServer extends Application<OaiConfig> {

  public static void main(String[] args) throws Exception {
    new OaiServer().run(args);
  }

  @Override
  public String getName() {
    return "OAI-PMH server";
  }

  public void run(OaiConfig cfg, Environment env) throws Exception {

    RepositoryInitializer initializer = new RepositoryInitializer(env);
    Map<String, StandAloneRepository<?>> repos = initializer.getRepositories(cfg.getRepositories());
    Map<String, RepositoryGroup<?>> repoGroups = initializer.getRepoGroups(cfg.getRepositoryGroups());

    env.jersey().register(new HomeResource());
    env.jersey().register(new RepositoryResource(repos, cfg.getBaseUrl()));
    env.jersey().register(new RepoGroupResource(repoGroups, cfg.getBaseUrl()));

  }

}
