package nl.naturalis.oaipmh.resource;

import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.openarchives.oai._2.OAIPMHtype;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.naturalis.oaipmh.api.OaiRepository;
import nl.naturalis.oaipmh.api.OaiRequest;
import nl.naturalis.oaipmh.api.RepositoryGroup;
import nl.naturalis.oaipmh.api.ResumptionTokenParser;
import nl.naturalis.oaipmh.api.XsdNotFoundException;
import nl.naturalis.oaipmh.util.HttpResponses;
import nl.naturalis.oaipmh.util.OaiRequestBuilder;
import nl.naturalis.oaipmh.util.RequestExecutor;

import static java.util.stream.Collectors.joining;

import static nl.naturalis.oaipmh.api.util.OaiUtil.createOAIPMHErrorResponse;
import static nl.naturalis.oaipmh.util.HttpResponses.notFound;
import static nl.naturalis.oaipmh.util.HttpResponses.serverError;
import static nl.naturalis.oaipmh.util.HttpResponses.streamingResponse;
import static nl.naturalis.oaipmh.util.HttpResponses.xmlResponse;

/**
 * REST resource handling requests for OAI repositories within a repository group.
 * 
 * @author Ayco Holleman
 *
 */
@Path("/{group}/{repo}")
public class RepoGroupResource {

  private static final Logger logger = LoggerFactory.getLogger(RepoGroupResource.class);

  private final Map<String, RepositoryGroup<?>> repoGroups;
  private final String baseUrl;

  @Context
  private UriInfo uriInfo;

  public RepoGroupResource(Map<String, RepositoryGroup<?>> repoGroup, String baseUrlOverride) {
    this.repoGroups = repoGroup;
    this.baseUrl = baseUrlOverride;
  }

  /**
   * Handles requests for repositories within a repository group.
   * 
   * @param groupName
   * @param repoName
   * @return
   */
  @GET
  public Response handleRequest(@PathParam("group") String groupName, @PathParam("repo") String repoName) {
    try {
      logRequest(groupName, repoName);
      RepositoryGroup<?> group = repoGroups.get(groupName);
      if (group == null) {
        return notFound(noSuchGroup(groupName));
      }
      OaiRepository repository = group.getRepository(repoName);
      if (repository == null) {
        return notFound(noSuchRepo(groupName, repoName));
      }
      ResumptionTokenParser parser = repository.getResumptionTokenParser();
      OaiRequestBuilder orb = new OaiRequestBuilder(parser, baseUrl, uriInfo);
      OaiRequest request = orb.build();
      if (orb.getErrors().size() != 0) {
        OAIPMHtype oaipmh = createOAIPMHErrorResponse(request, orb.getErrors());
        return xmlResponse(Status.BAD_REQUEST, oaipmh);
      }
      repository.newOaiRequest(request);
      return new RequestExecutor(repository, request).getResponse();
    } catch (WebApplicationException e) {
      return e.getResponse();
    } catch (Exception e) {
      return HttpResponses.serverError(e);
    }
  }

  /**
   * Returns the XSD for the specified metadataPrefix.
   * 
   * @param repoGroup
   * @param repoName
   * @param prefix
   * @return
   */
  @GET
  @Path("/{prefix}.xsd")
  public Response getXsd(@PathParam("group") String groupName, @PathParam("repo") String repoName, @PathParam("prefix") String prefix) {
    logRequest(groupName, repoName);
    RepositoryGroup<?> group = repoGroups.get(groupName);
    if (group == null) {
      return notFound(noSuchGroup(groupName));
    }
    OaiRepository repo = group.getRepository(repoName);
    if (repo == null) {
      return notFound(noSuchRepo(groupName, repoName));
    }
    try {
      return streamingResponse(repo.getXsdForMetadataPrefix(prefix), MediaType.APPLICATION_XML);
    } catch (XsdNotFoundException e) {
      return notFound(e.getMessage());
    } catch (Exception e) {
      return serverError(e);
    }
  }

  private static void logRequest(String groupName, String repoName) {
    logger.info("Finding OAI repository for \"{}/{}\"", groupName, repoName);
  }

  private String noSuchGroup(String groupName) {
    String available = repoGroups.keySet().stream().collect(joining(", "));
    return String.format("No such repository group: \"%s\". Available repository groups: %s", groupName, available);
  }

  private String noSuchRepo(String groupName, String repoName) {
    RepositoryGroup<?> group = repoGroups.get(groupName);
    String available = group.getRepositoryNames().stream().collect(joining(", "));
    return String.format("No such repository in group %s: \"%s\". Available repositories: %s", groupName, repoName, available);
  }

}
