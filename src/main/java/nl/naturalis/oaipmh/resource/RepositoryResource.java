package nl.naturalis.oaipmh.resource;

import java.util.Map;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import nl.naturalis.oaipmh.api.StandAloneRepository;

/**
 * REST resource handling requests for stand-alone OAI repositories.
 * 
 * @author Ayco Holleman
 *
 */
@Path("/{repo}")
@SuppressWarnings("unused") // TODO: implement once we got stand-alone repos (pretty much just like RepoGroupResource)
public class RepositoryResource {

  private static final Logger logger = LoggerFactory.getLogger(RepositoryResource.class);

  private final Map<String, StandAloneRepository<?>> repos;
  private final String baseUrl;

  @Context
  private UriInfo uriInfo;

  public RepositoryResource(Map<String, StandAloneRepository<?>> repos, String baseUrl) {
    this.repos = repos;
    this.baseUrl = baseUrl;
  }

  @GET
  public Response handleRequest(@PathParam("repo") String repoName) {
    return null;
  }

  @GET
  @Path("/{prefix}.xsd")
  public Response getXsd(@PathParam("repo") String repoName, @PathParam("prefix") String prefix) {
    return null;
  }

}
