package nl.naturalis.oaipmh.resource;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import nl.naturalis.oaipmh.util.BuildInfo;

import static nl.naturalis.oaipmh.util.HttpResponses.streamingResponse;

@Path("/")
public class HomeResource {

  private static String welcome;

  @Context
  private HttpServletRequest httpServletRequest;

  /**
   * Show some welcome content.
   * 
   * @return
   */
  @GET
  @Produces(MediaType.TEXT_HTML)
  public String welcome() throws IOException {
    if (welcome == null) {
      try (InputStream in = getClass().getResourceAsStream("welcome.html")) {
        String html = IOUtils.toString(in);
        html = StringUtils.replaceOnce(html, "${version}", BuildInfo.getInstance().getVersion());
        html = StringUtils.replaceOnce(html, "${buildDate}", BuildInfo.getInstance().getBuildDate());
        html = StringUtils.replaceOnce(html, "${commitCount}", BuildInfo.getInstance().getCommitCount());
        html = StringUtils.replaceOnce(html, "${gitBranch}", BuildInfo.getInstance().getGitBranch());
        html = StringUtils.replaceOnce(html, "${gitCommit}", BuildInfo.getInstance().getGitCommit());
        return welcome = html;
      }
    }
    return welcome;
  }

  /**
   * Intercepts requests for a favicon in case we are running under "/".
   * 
   * @return
   */
  @GET
  @Path("/favicon.ico")
  public Response favicon() {
    // return Response.noContent().build(); (just ignore the request)
    return streamingResponse(getClass().getResourceAsStream("/favicon.ico"), "image/x-icon");
  }

}
