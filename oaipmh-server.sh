#!/bin/bash

# Start/stop script for the OAI-PMH server

usage='USAGE: oaipmh-server.sh start [ /path/to/oaipmh-server.yaml ] | stop | status'
workdir=$(pwd)
libdir=$workdir/lib
main='nl.naturalis.oaipmh.OaiServer'

function getpid {
	ps -ef | grep java | grep $main | awk -v N=2 '{print $N}'
}

action=${1}

if [[ -z $action ]]; then
	echo $usage && exit 1
elif [[ $action = 'start' ]]; then
	if [[ ! -z $(getpid) ]]; then
		echo 'OAI-PMH server already up and running' && exit 1
	fi
	config=${2}
	if [[ -z $config ]]; then
		config="$workdir/oaipmh-server.yaml"
		if [[ ! -f $config ]]; then
			echo $usage
			echo 'Missing configuration file' && exit 1
		fi
	fi
	cp=$workdir
	for file in $(ls $libdir); do
		cp="$cp:$libdir/$file"
	done
	java "-Dfile.encoding=UTF-8" "-classpath" "${cp}" "$main" "server" "${config}" &
elif [[ $action = 'stop' ]]; then
	if [[ -z $(getpid) ]]; then
		echo 'OAI-PMH server already down' && exit 1
	fi
	kill $(getpid) &
	for i in '1 2 3 4 5'; do
		sleep 2
		[[ -z $(getpid) ]] && exit 0
	done
	echo 'OAI-PMH server did not terminate within 10 seconds. Will kill it now'
	[[ ! -z $(getpid) ]] && kill -9 $(getpid) && exit 1
elif [[ $action = 'status' ]]; then
	[[ -z $(getpid) ]] && echo 'OAI-PMH server down' && exit 0
	echo 'OAI-PMH server up and running' && exit 1
else
	echo $usage && exit 1
fi
